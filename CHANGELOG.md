## [1.1] - 2016-06-12
- Added polish - characters now speak a variety of phrases when calling companions
- Fixed sleeping companions not listening on first call

## [1.0] - 2016-06-11
- Added basic functionality
