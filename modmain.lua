local companions = {}
companions[1] = {tag="chester", inventory_item="chester_eyebone", names={"Chester", "buddy", "Mr. Otto von Chesterfield, Esquire"}}
companions[2] = {tag="glommer", inventory_item="glommerflower", names={"Glommer", "goofy bug", "fuzzball"}}
companions[3] = {tag="packim", inventory_item="packim_fishbone", names={"Packim", "Baggims", "silly bird"}}

local phrase_templates = {"{}", "Come along, {}", "Get over here {}", "Hey, {}"}

local punctuation = {".", "!"}

local function phrase_generator(names)
  local phrase = phrase_templates[math.random(#phrase_templates)] .. punctuation[math.random(#punctuation)]
  phrase = phrase:gsub("{}", names[math.random(#names)])
  phrase = phrase:sub(1, 1):upper() .. phrase:sub(2)
  return phrase
end

for index, companion in pairs(companions) do

  local function recall(inst)
    local companion_inst = TheSim:FindFirstEntityWithTag(companion.tag)
    -- create a delay for companion to allow time to wake if necessary, else they won't move
    local delay = .1

    if companion_inst then
      if companion_inst.components.sleeper and companion_inst.components.sleeper:IsAsleep() then
        companion_inst.components.sleeper:WakeUp()
        delay = .5
      end
      companion_inst:DoTaskInTime(delay, function() companion_inst.components.locomotor:GoToEntity(companion_inst.components.follower.leader, nil, true) end)
      local owner = inst.components.inventoryitem.owner
      if owner and owner.components.talker then
        owner.components.talker:Say(phrase_generator(companion.names), 2, false)
      end
    end
  end

  local function check_living()
    local companion_inst = TheSim:FindFirstEntityWithTag(companion.tag)
    if companion_inst == nil then
      return false
    elseif companion_inst and companion_inst.components.health:IsDead() then
      return false
    else
      return true
    end
  end

  local function obedience_attach(inst)
    inst:AddComponent("useableitem")
    inst.components.useableitem:SetOnUseFn(recall)
    inst.components.useableitem:SetCanInteractFn(check_living)
  end

  AddPrefabPostInit(companion.inventory_item, obedience_attach)

end
