name = "Obedient Companions"
author = "star_lion"
version = "1.1"
description = "Make your companions heed your call - right-click follower items to have companions return.\n\nVersion " .. version

forumthread = "" -- steam only
api_version = 6

icon = "modicon.tex"
icon_atlas = "modicon.xml"

dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true
